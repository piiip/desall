package Model

type User struct {
	UserId    int64  `json:"userId" form:"userId" sql:"userId"`
	UserName  string `json:"userName" form:"userName" sql:"userName"`
	Password  string `json:"passWord" form:"passWord" sql:"passWord"`
	Telephone string `json:"telephone" form:"telephone" sql:"telephone"`
	Sex       string `json:"sex" form:"sex" sql:"sex"`
}
