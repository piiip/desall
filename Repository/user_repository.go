package Repository

import (
	"awesomeProject14/common"
	"database/sql"
	"errors"
	"gitee.com/piiip/desall/Model"
)

type UserRep struct {
	db    *sql.DB
	table string
}

type UserRepository interface {
	Conn() (err error)
	AddUser(user *Model.User) (int64, error)
	DeleteUser(userId int64) (err error)
	SelectByName(userName string) (user *Model.User, err error)
}

func NewUserRepository(table string, db *sql.DB) UserRepository {
	return &UserRep{table: table, db: db}
}

func (UR *UserRep) Conn() (err error) {
	if UR.db == nil {
		mysql, err := common.NewMysqlConn()
		UR.db = mysql
		if err != nil {
			return err
		}
	}
	if UR.table == "" {
		UR.table = "user"
	}
	return nil
}

func (UR *UserRep) AddUser(user *Model.User) (int64, error) {
	if err := UR.Conn(); err != nil {
		return 0, nil
	}
	sql := "INSERT user SET userId=?,userName=?,passWord=?,telephone=?,Sex=?"
	stmt, errstmt := UR.db.Prepare(sql)
	if errstmt != nil {
		return 0, errstmt
	}
	res, errres := stmt.Exec(user.UserId, user.UserName, user.Password, user.Telephone, user.Sex)
	if errres != nil {
		return 0, errres
	}
	return res.LastInsertId()
}

func (UR *UserRep) DeleteUser(userId int64) (err error) {
	if err := UR.Conn(); err != nil {
		return err
	}
	sql := "Delete from user where userId = ?"
	stmt, errstmt := UR.db.Prepare(sql)
	if errstmt != nil {
		return errstmt
	}
	_, errres := stmt.Exec(userId)
	if errres != nil {
		return errres
	}
	return nil
}

func (UR *UserRep) SelectByName(userName string) (user *Model.User, err error) {
	if err = UR.Conn(); err != nil {
		return &Model.User{}, err
	}
	sql := "Select *from user where userName=?"
	row, errRow := UR.db.Query(sql)
	if errRow != nil {
		return &Model.User{}, errRow
	}
	res := common.GetResultRow(row)
	if len(res) == 0 {
		return &Model.User{}, errors.New("没有结果")
	}
	user = &Model.User{}
	common.DataToStructByTagSql(res, user)
	return
}
