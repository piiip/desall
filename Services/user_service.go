package Services

import (
	"gitee.com/piiip/desall/Model"
	"gitee.com/piiip/desall/Repository"
)

type UserSev struct {
	RU Repository.UserRep
}

type UserService interface {
	InsertUser(user *Model.User) (int64, error)
	DropUser(userId int64) (err error)
}

func NewUserService(userrepository Repository.UserRep) UserService {
	return &UserSev{userrepository}
}

func (US *UserSev) InsertUser(user *Model.User) (int64, error) {
	return US.RU.AddUser(user)
}

func (US *UserSev) DropUser(userId int64) (err error) {
	return US.RU.DeleteUser(userId)
}
